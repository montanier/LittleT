#!/usr/bin/python

# This file is part of LittleT.
# 
# LittleT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# LittleT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LittleT.  If not, see <http://www.gnu.org/licenses/>.

import socket
import sys
import sys, tty, termios

def getch():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch

ip = sys.argv[1]

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
server_address = (ip, 8888)
sock.connect(server_address)

loop = True
while loop == True:
    ch = getch()
    if ord(ch) == 27:
        loop = False
    else:
        sock.sendall(ch+"\n") 
        data = sock.recv(1024)
        print(data)

sock.close()

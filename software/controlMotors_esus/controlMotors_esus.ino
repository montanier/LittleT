#include <esusBoard.h>

/*
This file is part of LittleT.

LittleT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LittleT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LittleT.  If not, see <http://www.gnu.org/licenses/>.
*/

void setup() 
{
  // init esus board 
  initEsusBoard();
  
}

void loop() 
{
  // control motor 1, direction = forward
 motors1_set(512, DIR_FORWARD);

 // control motor 2, direction = forward
 motors2_set(512, DIR_FORWARD);

 // 1 second delay
 delay(1000);

 // control motor 1, direction = forward
 motors1_set(1024, DIR_FORWARD);

 // control motor 2, direction = forward
 motors2_set(1024, DIR_FORWARD);

 // 1 second delay
 delay(1000);

 
  // control motor 1, direction = forward
 motors1_set(512, DIR_FORWARD);

 // control motor 2, direction = forward
 motors2_set(512, DIR_FORWARD);

 // 1 second delay
 delay(1000);

// control motor 1, direction = forward
 motors1_set(0, DIR_FORWARD);

 // control motor 2, direction = forward
 motors2_set(0, DIR_FORWARD);

 // 0.5 second delay
 delay(500);

// control motor 1,direction = back
 motors1_set(512, DIR_BACK);

 // control motor 2,direction = back
 motors2_set(512, DIR_BACK);

 delay(1000);

 // control motor 1,direction = back
 motors1_set(1024, DIR_BACK);

 // control motor 2,direction = back
 motors2_set(1024, DIR_BACK);

 delay(1000);
 
// control motor 1,direction = back
 motors1_set(512, DIR_BACK);

 // control motor 2,direction = back
 motors2_set(512, DIR_BACK);

 delay(1000);
 
// control motor 1, direction = forward
 motors1_set(0, DIR_FORWARD);

 // control motor 2, direction = forward
 motors2_set(0, DIR_FORWARD);

 // 0.5 second delay
 delay(500); 
}

#include <ESP8266WiFi.h>
#include <esusBoard.h>
#include <math.h>

/*
This file is part of LittleT.

LittleT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LittleT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LittleT.  If not, see <http://www.gnu.org/licenses/>.
*/

#define SSID        "electrolab"
#define PASSWORD    "plopplop"

WiFiServer server(8888);
void setup() 
{
  initEsusBoard();
  initHardware();
  setupWiFi();
  server.begin();
}

WiFiClient clientTcp;
int m1_speed = 0;
int m2_speed = 0;
void loop() 
{
    if (!clientTcp.connected()) {
        // try to connect to a new client
        clientTcp = server.available();
    } 
    else 
    {
        // read data from the connected client
        if (clientTcp.available() > 0) {
            String message = clientTcp.readStringUntil('\n');
            Serial.print(message+"\n");
            if (message == "z")
            {
              Serial.print(String(m1_speed)+":"+String(m2_speed)+"\n");
              if (m1_speed != m2_speed)
              {
                m1_speed = _max(m1_speed,m2_speed);
                m2_speed = m1_speed;
              }
              else
              {
                m1_speed += 1024;
                m2_speed += 1024;
              }
              Serial.print(String(m1_speed)+":"+String(m2_speed)+"\n");
            }
            
            if (message == "s")
            {
              if (m1_speed != m2_speed)
              {
                m1_speed = _min(m1_speed,m2_speed);
                m2_speed = m1_speed;
              }
              else
              {
                m1_speed -= 1024;
                m2_speed -= 1024;
              }
            }
            
            if (message == "q")
            {
               m1_speed = 1024;
               m2_speed = 0;
            }
            
            if (message == "d")
            {
               m1_speed = 0;
               m2_speed = 1024;
            }
            
            if (m1_speed > 1024) m1_speed = 1024;
            if (m1_speed < -1024) m1_speed = -1024;
            if (m2_speed > 1024) m2_speed = 1024;
            if (m2_speed < -1024) m2_speed = -1024;

            if (m1_speed < 0) motors1_set(abs(m1_speed), DIR_BACK);
            else motors1_set(m1_speed, DIR_FORWARD);
            
            if (m2_speed < 0) motors2_set(abs(m2_speed), DIR_BACK);
            else motors2_set(m2_speed, DIR_FORWARD);
            
            clientTcp.write((const uint8_t*)message.c_str(),message.length());
        }
        
    }
}


void setupWiFi()
{
  WiFi.begin(SSID,PASSWORD);
  Serial.println("");
  //Wait for connection
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("Connected to "); Serial.println(SSID);
  Serial.print("IP Address: "); Serial.println(WiFi.localIP());
}

void initHardware()
{
  Serial.begin(115200);
}

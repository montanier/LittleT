Software directory
==================

A description of what can be found in each sub-directory of the software directory

# controlMotors_esus

Some tests to control the motors without any feedback of the wifi or sensors. Usefull to check that the motors and power boards are wired correctly.

# esp8266

Set of libraries for the esp8266. Not necessary but it was usefull for me to understand how the wifi setting were working.

# tcpServer

The arduino code to turn the Esus board as a server. It listen to charcater received and drive the motors accordingly.

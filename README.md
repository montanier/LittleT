Little-T: Little Teleoperated Robot
==================================

# Description
Name: Little-T

Purpose: Just playing to do a little robot

A Simple autonomous platform that can be teleoperated. Explored the control of a robot, using a wifi connection, a charging solution.

## Outcome

You can take a look at the video on the wiki to get an idea of the robot assembled.

The esus board is a good idea to get a wifi control and pilot motors. The sparkfun boad is a nice one for the power of the robot because one can easily use the robot on battery or power while loading the robot.

For the SCAD I tried freecad and openscad. In the end I prefer openscad for the following reasons:
- simple modifications in freecad tend to break references in other graphs. In mycase, a modification in the hight of the base was breaking references for the parts holding the motors. With openscad, I can control myself the references.
- I'm at ease with the code, which make the use of openscad easy for me. For exmaple, I redesigned my base in about 1h30.

I think I may change for a graphic tool if it was more powerful than freecad.

## Next step

I could add a third wheel to stabilize the robot, but I'm a bit bored by that kind of robot. I will do a self-stabilizing robot. For that I will need to:
* re-do the base
* get an accelerometer/gyroscope
* try out more complex code for self-balancing

# Material 

## Computation
Esus Board: https://www.macerobotics.com/esus-board/

## Motors
*  GM8 Solarbotics (robotshop)

## Battery
* LiPo 3.7V 1200 mAh
* SparkFun USB LiPoly Charger. Based on MCP73831T

## Base

3D print the stl file found in hardware/drawing-base.

Original source file (openscad) is found in hardware/drawing-base/base.scad. The first drawings were done with freecad. The original fcstd file are kept.

# Assembly

# Control

* Modify server's settings to match your wifi network: software/tcpServer/tcpServer.ino
* Upload the code of the tcp server on the board
* Read IP of the board on your network from the console provided by the Arduino software
* Start python client: ./tcpClient.py IP_BOARD
* User the zqsd touches (Azerty layout, aka french keyboard) to drive the robot

/*
This file is part of LittleT.

LittleT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LittleT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LittleT.  If not, see <http://www.gnu.org/licenses/>.
*/


module holes_conf1() //esus + charger + sparkMini
{
    module holes_esus()
    {
        width=44;
        height=74;
        translate([0,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([0,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
    }

    module holes_charger()
    {
        width=33.05;
        height=30.84;
        translate([0,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([0,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
    }

    module holes_sparkMini()
    {
        height=37;
        translate([0,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([0,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
    }

    translate([9,13,0])holes_esus();
    translate([60,61,0])holes_charger();
    translate([79,8,0])holes_sparkMini();
}


module holes_conf2() //PiZero + charger + breadBoard
{
    module holes_piZero()
    {
        width=23;
        height=58;
        translate([0,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([0,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
    }

    module holes_charger()
    {
        width=33.05;
        height=30.84;
        translate([0,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([0,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
    }

    module holes_breadBoard()
    {
        width=33.5;
        height=38.5;
        translate([0,0,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([0,height,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
        translate([width,height/2,-0.1])cylinder(6.2,1.5,1.5,$fn=20);
    }

    translate([9,21,0])holes_piZero();
    translate([60,61,0])holes_charger();
    translate([59,6,0])holes_breadBoard();
}



module motor_support(side)
{
    
    difference()
    {
        cube([3,34,32]);
        translate([-0.1,9,10])rotate([0,90,0])cylinder(3.2,2,2,$fn=20);
        translate([-0.1,9,28])rotate([0,90,0])cylinder(3.2,2,2,$fn=20);
        translate([-0.1,17,19])rotate([0,90,0])cylinder(3.2,4,4,$fn=20);
        translate([-0.1,28,19])rotate([0,90,0])cylinder(3.2,2.5,2.5,$fn=20);
    }
    
    
    difference()
    {
        translate([1.5,-5,5])rotate([0,90,0])cube([10,10,3],center=true);
        translate([-1.5,-10,10])rotate([0,90,0])cylinder(10,10,10,$fn=100);
    }
    
    
    if (side == "right")
    {
        difference()
        {
            translate([3,0,0])cube([8,34,8]);
            translate([11,35,8])rotate([90,90,0])cylinder(36,8,8,$fn=100);
        }
    }
    
    if (side == "left")
    {
        translate([3,34,0])
        rotate([0,0,180])
        difference()
        {
            translate([3,0,0])cube([8,34,8]);
            translate([11,35,8])rotate([90,90,0])cylinder(36,8,8,$fn=100);
        }
    }
    
    difference()
    {
        translate([1.5,39,5])rotate([0,90,0])cube([10,10,3],center=true);
        translate([-1.5,44,10])rotate([0,90,0])cylinder(10,10,10,$fn=100);
    }
}


module battery_support()
{
    height=5;
    width=36;
    depth=15;
    cube([1.5,depth,height]);
    translate([width,0,0])cube([1.5,depth,height]);
    translate([0,0,height])cube([width+1.5,depth,2]);
}

union()
{
    difference()
    {
        
        cube([100,100,6]);
        color("red",1.0){holes_conf1();}
        color("blue",1.0){holes_conf2();}
    }
    translate([0,33,6])motor_support("right");
    translate([97,33,6])motor_support("left");
    translate([32,23,6])battery_support();
}
